<?php
/*
 * Plugin Name: chimpo
 * Description: PDG Mailchimp integration plugin
 * Version 1.0
 * Author: Treighton @ Page Design Group
 * License:
 * */

if ( ! defined( 'ABSPATH' ) ) {
    die;
}

function mc_campaigns( $apikey, $server ) {

    $auth = base64_encode( 'user:'. $apikey );


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'api.mailchimp.com/3.0/campaigns');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        'Authorization: Basic '. $auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


    $campaigns = curl_exec($ch);

    $campaigns = json_decode($campaigns);

    $campaigns = $campaigns -> campaigns;
    if (is_array($campaigns)){
        foreach ($campaigns as $campaign) { ?>
            <option type="checkbox" value="<?php echo $campaign -> id ?>" ><? $settings = $campaign -> settings;  echo $settings -> subject_line;   ?></option> <?php
        }
    }


}

function mc_lists( $apikey, $server ) {

    $auth = base64_encode( 'user:'. $apikey );


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'api.mailchimp.com/3.0/lists');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        'Authorization: Basic '. $auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


    $lists = curl_exec($ch);

    $lists = json_decode($lists);

    return $lists;

}

function mc_templates( $apikey, $server ) {

    $auth = base64_encode( 'user:'. $apikey );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'api.mailchimp.com/3.0/templates');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        'Authorization: Basic '. $auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $templates = curl_exec($ch);

    $templates = json_decode($templates);

    return $templates;

}

function mc_chimpo($subject) {
    $options = get_option( 'chimpo_settings' );
    $apikey = $options['chimpo_text_field_0'];
    $server = $options['chimpo_text_field_3'];
    $from = $options['chimpo_text_field_2'];
    $reply = $options['chimpo_text_field_1'];
    $list =$options['chimpo_lists'];

    $auth = base64_encode( 'user:'. $apikey );

    $data = array(
        'recipients' => array(
            'list_id' => $list
        ),
        'type' => 'regular',
        'settings' => array(
            'subject_line' => $subject,
            'reply_to' => $reply,
            'from_name' => $from,
        )

    );

    $json_data = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'api.mailchimp.com/3.0/campaigns');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        'Authorization: Basic '. $auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);



    $camp_id = curl_exec($ch);

    $camp_id = json_decode($camp_id);

    $camp_id = $camp_id -> id;

   return $camp_id;
}

function mc_chimpo_camp($camp_id, $content, $image, $title, $template) {
    $options = get_option( 'chimpo_settings' );

    $apikey = $options['chimpo_text_field_0'];
    $server = $options['chimpo_text_field_3'];

    $auth = base64_encode( 'user:'. $apikey );

    $featimage = '<img src="'. $image .'" />';
    $data = array(

        'template' => array(
            'id' => $template,
            'sections' => array (
                'title' => $title,
                'body' => $content,
                'featimage' => $featimage
                )
            )

    );

    $json_data = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'api.mailchimp.com/3.0/campaigns/'.$camp_id.'/content');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        'Authorization: Basic '. $auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);



    $lists = curl_exec($ch);
    return $lists;
}

add_action( 'admin_menu', 'chimpo_add_admin_menu' );
add_action( 'admin_init', 'chimpo_settings_init' );

function chimpo_options_page(  ) {

    ?>
    <form action='options.php' method='post'>

        <h1>Chimpo</h1>

        <?php
        settings_fields( 'pluginPage' );
        do_settings_sections( 'pluginPage' );
        submit_button();
        ?>

    </form>

<?php }

function chimpo_add_admin_menu(  ) {
    add_menu_page( 'Chimpo', 'Chimpo', 'manage_options', 'chimpo', 'chimpo_options_page' );
}

function chimpo_settings_init(  ) {
    register_setting( 'pluginPage', 'chimpo_settings' );
    add_settings_section(
        'chimpo_pluginPage_section',
        __( 'Mailchimp API Settings', 'wordpress' ),
        'chimpo_settings_section_callback',
        'pluginPage'
    );

    add_settings_field(
        'chimpo_text_field_0',
        __( 'API Key', 'wordpress' ),
        'chimpo_text_field_0_render',
        'pluginPage',
        'chimpo_pluginPage_section'
    );

    add_settings_field(
        'chimpo_text_field_1',
        __( 'Email Address', 'wordpress' ),
        'chimpo_text_field_1_render',
        'pluginPage',
        'chimpo_pluginPage_section'
    );

    add_settings_field(
        'chimpo_text_field_2',
        __( 'Name', 'wordpress' ),
        'chimpo_text_field_2_render',
        'pluginPage',
        'chimpo_pluginPage_section'
    );

    add_settings_field(
        'chimpo_text_field_3',
        __( 'Server', 'wordpress' ),
        'chimpo_text_field_3_render',
        'pluginPage',
        'chimpo_pluginPage_section'
    );

    add_settings_field('chimpo_lists',
    __('Default List', 'wordpress'),
    'chimpo_list_field_render',
        'pluginPage',
        'chimpo_pluginPage_section');

    add_settings_field('chimpo_templates',
        __('Default Template', 'wordpress'),
        'chimpo_template_field_render',
        'pluginPage',
        'chimpo_pluginPage_section');

}


function chimpo_text_field_0_render(  ) {

    $options = get_option( 'chimpo_settings' );
    ?>
    <input type='text' name='chimpo_settings[chimpo_text_field_0]' value='<?php echo $options['chimpo_text_field_0']; ?>'>
    <?php

}


function chimpo_text_field_1_render(  ) {

    $options = get_option( 'chimpo_settings' );
    ?>
    <input type='text' name='chimpo_settings[chimpo_text_field_1]' value='<?php echo $options['chimpo_text_field_1']; ?>'>
    <?php

}


function chimpo_text_field_2_render(  ) {

    $options = get_option( 'chimpo_settings' );
    ?>
    <input type='text' name='chimpo_settings[chimpo_text_field_2]' value='<?php echo $options['chimpo_text_field_2']; ?>'>
    <?php

}


function chimpo_text_field_3_render(  ) {

    $options = get_option( 'chimpo_settings' );
    ?>
    <input type='text' name='chimpo_settings[chimpo_text_field_3]' value='<?php echo $options['chimpo_text_field_3']; ?>'>
    <?php

}

function chimpo_list_field_render( ){
    $options = get_option( 'chimpo_settings' );

    ?>
    <input type='text' name='chimpo_settings[chimpo_lists]' value='<?php echo $options['chimpo_lists']; ?>'>
    <select name="chimpo_settings[chimpo_lists]" id="lists">
        <?php
        $apikey = $options['chimpo_text_field_0'];
        $server = $options['chimpo_text_field_3'];

        $lists = mc_lists( $apikey, $server );

        $lists = $lists -> lists;

        foreach ($lists as $list ) {
            ?> <option value="<?php echo $list -> id ?>"><?php echo $list -> name ?></option> <?php
        } ?>
    </select>
    <?php
}

function chimpo_template_field_render( ){
    $options = get_option( 'chimpo_settings' );

    ?>
    <input type='text' name='chimpo_settings[chimpo_templates]' value='<?php echo $options['chimpo_templates']; ?>' disabled>

    <select name="chimpo_settings[chimpo_templates]" id="templates">
        <?php
        $apikey = $options['chimpo_text_field_0'];
        $server = $options['chimpo_text_field_3'];

        $templates = mc_templates( $apikey, $server );

        $templates = $templates -> templates;

        foreach ($templates as $template ) {
            ?> <option value="<?php echo $template -> id ?>"><?php echo $template -> name ?></option> <?php
        } ?>
    </select>
    <?php
}

function chimpo_settings_section_callback(  ) {

    echo __( 'This section description', 'wordpress' );

}




add_action( 'add_meta_boxes', 'chimpo_add_custom_box' );
function chimpo_add_custom_box() {
    $screens = array( 'post' );
    foreach ( $screens as $screen ) {
        add_meta_box(
            'chimpo',            // Unique ID
            'PDG Chimpo',      // Box title
            'chimpo_box',  // Content callback
            $screen                      // post type
        );
    }
}

/* Prints the box content */
function chimpo_box( $post ) {
    $chimpDefaults = !! get_post_meta( $post->ID, 'chimpDefaults', true );
    ?>
    <label for="chimpDefaults"> Chimp this? </label>
    <input id="chimpDefaults" name="chimpDefaults" type="checkbox" <?php checked( $chimpDefaults) /* WordPress helper function */ ?> />
    <input type="hidden" name="do_chimpDefaults" value="1" />
    <div class="chimpOption" style="display: none">
        <h2>Chmipo Default Options</h2>
        <p>These can be changed or updated in the Chimpo admin panel</p>
        <ul class="defaults">
            <?php $options = get_option( 'chimpo_settings' ); ?>
            <li><strong>From Name: </strong><?php echo $options['chimpo_text_field_2'] ?></li>
            <li><strong>Reply to: </strong><?php echo $options['chimpo_text_field_1'] ?></li>
            <li><strong>Default List: </strong><?php echo $options['chimpo_lists'] ?></li>
            <li><strong>Default Template: </strong><?php echo $options['chimpo_templates'] ?></li>
        </ul>
    </div>
    <script>
        (function($) {

            $('#chimpDefaults').on('change', function(){
                $('.chimpOption').toggle();

            })

        })( jQuery );
    </script>
    <?php
}

function chimpo_save_status($post_id) {
    if ( isset( $_POST['chimpDefaults'] ) ) {
        // Use isset - unchecked checkboxes won't send a value to $_POST, you'll get an undefined index error
        update_post_meta( $post_id, 'chimpDefaults', isset( $_POST['chimpDefaults'] ) ? '1' : '0' );
    }



}

add_action( 'save_post', 'chimpo_save_status' );


function chimpo_api( $new_status, $old_status, $post ) {
    if ( $old_status == 'draft' && $new_status == 'publish' ) {

        $subject = get_the_title( $post );

        $content = get_post_field('post_content', $post);

        $content = apply_filters('the_content', $content);

        $title = get_post_field('post_title', $post);

        $options = get_option( 'chimpo_settings' );

        $template = intval($options['chimpo_templates']);

        $image = wp_get_attachment_url( $post );

        $is_autosave = wp_is_post_autosave( $post );
        $is_revision = wp_is_post_revision( $post );

        // exit depending on the save status
        if ( $is_autosave || $is_revision ) {
            return;
        } elseif ( isset( $_POST['chimpDefaults'] ) && !wp_is_post_revision($post) ) {
            $camp_id = mc_chimpo($subject);
            mc_chimpo_camp($camp_id, $content, $image, $title, $template);
        } else {
            return;
        }

    }
}
add_action( 'transition_post_status', 'chimpo_api', 10, 3 );